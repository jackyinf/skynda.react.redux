const translationsObject = {
  et: {
    all: "Kõik",
    components: {
      car_search: {
        brand: "Automark",
        mileage: "Läbisõit",
        price: "Hind",
        year: "Aasta",
        colors: "Värvid",
        petrol_consumption: "Kütusekulu",
        features: "Lisad",
        power: "Auto Võimsus",
        doors: "Uksed",
        seats: "Istekohad",
        transmission: "Käigukast",
        advanced_txt: "Täpsemad valikud",
        btn_search: "Näita selliseid autosid",
        automatic: "Automaatne",
        manual: "Manuaal"
      },
      car_preview: {
        doors: "ust",
        seats: "isekohta"
      },
      footer: {
        your_company: "© Your Company",
        home: "Home",
        privacy: "Privacy",
        not_found: "Not Found"
      },
      header: {
        sell_car_txt: "Müün auto",
        buy_car_txt: "Ostan auto",
        about_us: "Meist",
        contacts: "Kontaktid"
      },
      navigation: {
        about: "Meist",
        contact: "Kontakt",
        log_in: "Logi Sisse",
        sign_up: "Liitu",
        log_out: "Logi Välja"
      }
    },

    home_page: {
      search: "Oma uue auto leiad siit",
      searching: "Otsin",
      found: "Leiti",
      populars: "Viimase aja popimad mudelid",
      recently_added: "Hiljuti valikusse lisandunud",
      hero: {
        main_text: "Kasutatud auto ostmine pole kunagi olnud nii lihtne, turvaline ja glamuurne.",
        read_more: "Kuidas nii?"
      }

    },
    details: {
      other_txt: "Veel Pakkumisi",
      components: {
        main_image: {
          btn_txt_view_photos: "Vaata Pilte",
          btn_txt_360: "360° Sisemusest"
        },
        overview: {
          header: "Üldandmed"
        },
        inspector_report: {
          header: "Sertifitseeritud Inspektori Raport",
          question: "Kas on küsimusi?",
          show_all: "Näita rohkem",
          send_question: "Saada"
        },
        checkout_panel: {
          first_name: "Eesnimi",
          last_name: "Perekonnanimi",
          email: "E-Posti Aadress",
          phone: "Mobiiltelefoni number",
          btn_send: "Saada",
          contact_24h_txt: "Võtame sinuga ühendust 24h jooksul.",
          contact_us_txt: "Võta Meiega Ühendust"
        },
        features: {
          header: "Mugavusvarustus"
        },
        history: {
          header: "Auto ajalugu",
          problems_found: "Leitud probleemid",
          no_problems_found: "Probleeme ei ole",
          vin: "VIN kood"
        },
        performance: {
          header: "Tehnilised Andmed",
          wheel_drive: "Vedav sild",
          fuel_type: "Kütus",
          horsepower: "Võimsus",
          compression: "Surveaste",
          engine_size: "Mootori Maht",
          compressor_type: "Survetüüp",
          torque: "Pöördemoment",
          config: "Seadistus",
          valves: "Klappide arv",
          cylinders: "Silindrid",
          powertrain: "Powertrain?",
          displacement: "Aasta"
        },
        petrol: {
          header: "Kütusekulu",
          city: "Linn",
          highway: "Maantee",
          average: "Keskmine"
        },
        reviews: {
          header: "Arvustused",
          btn_read_more: "Loe rohkem"
        },
        safety: {
          header: "Turvalisus"
        }

      }
    }
  },
  en: {
    all: "All",
    components: {
      car_search: {
        brand: "Brand",
        mileage: "Mileage",
        price: "Price",
        year: "Year",
        colors: "Colors",
        petrol_consumption: "Petrol consumption",
        features: "Features",
        power: "Horsepower",
        doors: "Doors",
        seats: "Seats",
        transmission: "Transmission"
      },
      car_preview: {
        doors: "doors",
        seats: "seats"
      },
      footer: {
        your_company: "Skynda AS",
        address: "Valge 16, Tallinn 19095 Estonia",
        email: "hello@skynda.me",
        phone: "+372 5144 750"
      },
      header: {
        sell_car_txt: "Sell Your Car",
        buy_car_txt: "Buy Your Car"
      },
      navigation: {
        about: "About",
        contact: "Contact",
        log_in: "Log In",
        sign_up: "Sign Up",
        log_out: "Log Out"
      }
    },
    home_page: {
      search: "Search",
      searching: "Searching",
      found: "Found",
      recently_added: "Recently Added",
      hero: {
        main_text: "Buying a pre-used car has never been so fun, easy and secure",
        read_more: "Read more"
      }

    },
    details: {
      components: {
        main_image: {
          btn_txt_view_photos: "View Photos",
          btn_txt_360: "360 View"
        },
        overview: {
          header: "Overview"
        },
        inspector_report: {
          header: "Our Certified Inspector's Report",
          question: "Have a question?",
          show_all: "Show all"
        },
        checkout_panel: {
          first_name: "First Name",
          last_name: "Last Name",
          email: "E-Mail",
          phone: "Phone Number",
          btn_send: "Send",
          contact_24h_txt: "We will contact you in 24h.",
          contact_us_txt: "Contact Us"

        },
        features: {
          header: "Nice features"
        },
        history: {
          header: "Car History",
          problems_found: "Problems found",
          no_problems_found: "No problems found",
          vin: "VIN code"
        },
        performance: {
          wheel_drive: "Wheels Driven",
          fuel_type: "Fuel Type",
          horsepower: "Horsepower",
          compression: "Compression Ratio",
          engine_size: "Engine Size",
          compressor_type: "Compressor Type",
          torque: "Torque",
          config: "Configuration",
          valves: "Total valves",
          cylinders: "Cylinders",
          powertrain: "Powertrain",
          displacement: "Displacement"
        },
        petrol: {
          city: "City",
          highway: "Highway",
          average: "Average"
        },
        reviews: {
          header: "Reviews",
          btn_read_more: "Read More"
        },
        safety: {
          header: "Safety"
        }

      }
    }
  }
};

export default translationsObject;
