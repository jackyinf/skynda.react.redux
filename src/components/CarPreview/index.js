import Item from "./CarPreviewItem";
import Grid from "./CarPreviewGrid";

export default {
  Item,
  Grid
};
