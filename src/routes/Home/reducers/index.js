import { searchBtn, toggleBtn } from "./searchBtnReducers";
import { setValues, loadBaseData } from "./searchReducer";
import onSliderChange from "./sliderReducer";

export {
  searchBtn,
  toggleBtn,
  setValues,
  loadBaseData,
  onSliderChange
};
