/**
 * Created by jevgenir on 10/26/2016.
 */

export const ACTIONS = {
  SET_CAR_MANUFACTURERS_DATA: "CAR_MANUFACTURERS/SET_CAR_MANUFACTURERS_DATA"
};

export const REDUCER_KEYS = {
  CAR_MODELS_DATA: "carModelsData"
};

export const ROUTE_PARAMS = {
  CAR_MANUFACTURER_ID: "carManufacturerId"
};
