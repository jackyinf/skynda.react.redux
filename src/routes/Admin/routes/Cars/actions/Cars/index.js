/**
 * Created by jevgenir on 10/24/2016.
 */
import getList from "./Cars.getList.action";
import deleteItem from "./Cars.deleteItem.action";

export {
  getList,
  deleteItem
}
