/**
 * Created by zekar on 10/23/2016.
 */
import {ACTIONS} from "./../../constants/Car.constant";

export default (value) => ({
  type: ACTIONS.SET_FORM_MODE,
  payload: value
});
