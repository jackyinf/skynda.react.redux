/**
 * Created by zekar on 10/23/2016.
 */

import clear from "./Car.clear.action";
import load from "./Car.load.action";
import setCarData from "./Car.setCarData.action";
import setFormMode from "./Car.setFormMode.action";
import submitCarForm, {createCarAsync} from "./Car.submitCarForm.action";
import fillWithFakeData from "./Car.fillWithFakeData.action";

export {
  clear,
  load,
  setCarData,
  setFormMode,
  submitCarForm,
  fillWithFakeData,
  createCarAsync
}
