/**
 * Created by jevgenir on 10/26/2016.
 */

export const ACTIONS = {
  SET_CAR_MODELS_DATA: "CAR_MODEL/SET_CAR_MODELS_DATA"
};

export const REDUCER_KEYS = {
  CAR_MODELS_DATA: "carModelsData"
};

export const ROUTE_PARAMS = {
  CAR_MODEL_ID: "carModelId"
};
